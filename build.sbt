name := "reactive-play"

organization := "pl.jborkowski"

version := "1.0.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  specs2 % Test
)

libraryDependencies += "com.ning" % "async-http-client" % "1.9.29"

libraryDependencies += "com.typesafe.play.extras" %% "iteratees-extras" % "1.5.0"


resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

resolvers += "TypeSafe private" at "https://private-repo.typesafe.com/typesafe/maven-relases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
