package controllers

import play.api.Play.current
import play.api.libs.json.{JsObject, JsValue}
import play.api.mvc._


class Application extends Controller {

  def index = Action { implicit request =>
    Ok(views.html.index("Tweets"))
  }

  import actors.TwitterStreamer

  def tweets = WebSocket.acceptWithActor[String, JsValue] {
    request => out => TwitterStreamer.props(out)
  }

  def replicateFeed = Action { implicit request =>
    Ok.feed(TwitterStreamer.subscribeNode)
  }

 /* import play.api.libs.iteratee._
  import play.api.Logger

  def tweets = Action.async {

    credentials.map { case (consumerKey, requestToken) =>


  }*/



}
